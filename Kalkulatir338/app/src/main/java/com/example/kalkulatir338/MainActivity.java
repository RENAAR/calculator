package com.example.kalkulatir338;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActionBar;
import android.content.ContextWrapper;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Калькулятор");
        text1 = findViewById(R.id.textView1);


    }


    TextView text1;
    String text0;
    String textAfter;
    String action = "Nothing";


    public void Insert(Integer number) {
        if (!(text1.getText().length() > 21)) {
            if (text1.getText().equals("0") || text1.getText().equals("Ошибка")) {
                text1.setText(number.toString());
            } else if (!text1.getText().equals("0")) {
                text1.setText((text1.getText() + number.toString()));
            }
        }
    }

    public void Clear(View view) {
        text1.setText("0");
    }

    public void Add1(View view) {
        Insert(1);
    }

    public void Add7(View view) {
        Insert(7);
    }

    public void Add9(View view) {
        Insert(9);
    }

    public void Add8(View view) {
        Insert(8);
    }

    public void Add4(View view) {
        Insert(4);
    }

    public void Add5(View view) {
        Insert(5);
    }

    public void Add6(View view) {
        Insert(6);
    }

    public void Add2(View view) {
        Insert(2);
    }

    public void Add3(View view) {
        Insert(3);
    }

    public void Add0(View view) {
        Insert(0);
    }

    public void AddRazdel(View view) {
        if (!text1.getText().equals("Ошибка")) {
            if (String.valueOf((text1.getText())).indexOf('.') < 0) {
                text1.setText((text1.getText() + "."));
            }
        }
    } //Добавляет запятушку

    public void AddUnarMinus(View view) {
        if (!text1.getText().equals("Ошибка")) {
            if (String.valueOf((text1.getText())).indexOf('.') < 0) {
                Integer a;
                a = Integer.valueOf(text1.getText().toString());
                a = -a;
                text1.setText(a.toString());
            } else {
                Float a;
                a = Float.valueOf(text1.getText().toString());
                a = -a;
                text1.setText(a.toString());
                Delete(text1.getText().toString());
            }
        }
    }

    public void Delete(String numberText) {
        numberText = text1.getText().toString().substring(text1.length() - 2, text1.length());
        if (numberText.equals(".0")) {
            text1.setText(text1.getText().toString().substring(0, text1.length() - 2));
        }
    }

    public void DeleteNumber(View view) {

        if (text1.getText().length() == 2 && Float.valueOf(text1.getText().toString()) < 0) //Удаление отрицательного числа в рязряде единиц
        {
            text1.setText(text1.getText().toString().substring(0, text1.length() - 2)); //Удаляет два значения
        } else if (text1.getText().equals("Ошибка")) {
            text1.setText("0");
        } else {
            text1.setText(text1.getText().toString().substring(0, text1.length() - 1));
        }
        if (text1.getText().equals("")) {
            text1.setText("0");
        }
    }

    public void PlusNumber(View view) {
        if (!text1.getText().equals("Ошибка")) {
            text0 = text1.getText().toString();
            text1.setText("0");
            action = "Plus";
        }
    }

    public void EqualsNumbers(View view) {
        if (!text1.getText().equals("Ошибка")) {
            switch (action) {
                case "AfterDegree":
                    text1.setText(Degree(text1.getText().toString(), textAfter));
                    Delete(text1.getText().toString());
                    break;
                case "AfterPlus":
                    text1.setText(Sum(textAfter, text1.getText().toString()));
                    Delete(text1.getText().toString());
                    break;
                case "AfterMinus":
                    text1.setText(Min(text1.getText().toString(), textAfter));
                    Delete(text1.getText().toString());
                    break;
                case "AfterSep":
                    text1.setText(Sep(text1.getText().toString(), textAfter));
                    Delete(text1.getText().toString());
                    break;
                case "AfterMul":
                    text1.setText(Mul(textAfter, text1.getText().toString()));
                    Delete(text1.getText().toString());
                    break;
                case "Plus":
                    textAfter = text1.getText().toString();
                    text1.setText(Sum(text0, text1.getText().toString()));
                    Delete(text1.getText().toString());
                    action = "AfterPlus";
                    break;
                case "Minus":
                    textAfter = text1.getText().toString();
                    text1.setText(Min(text0, text1.getText().toString()));
                    Delete(text1.getText().toString());
                    action = "AfterMinus";
                    break;
                case "Sep":
                    textAfter = text1.getText().toString();
                    text1.setText(Sep(text0, text1.getText().toString()));
                    Delete(text1.getText().toString());
                    action = "AfterSep";
                    break;
                case "Mul":
                    textAfter = text1.getText().toString();
                    text1.setText(Mul(text0, text1.getText().toString()));
                    Delete(text1.getText().toString());
                    action = "AfterMul";
                    break;
                case "Degree":
                    textAfter = text1.getText().toString();
                    text1.setText(Degree(text0, text1.getText().toString()));
                    Delete(text1.getText().toString());
                    action = "AfterDegree";
                    break;



                default:
                    text1.setText(text1.getText());
                    break;
            }
        }
    }

    private String Degree(String numberText1, String numberText2) {
        Double number1, number2, result;
        String resultText;
        number1 = Double.valueOf(numberText1);
        number2 = Double.valueOf(numberText2);

        result = Math.pow(number1, number2);
        resultText = String.valueOf(result);

        if (resultText.equals("NaN") || resultText.equals("Infinity") || resultText.equals("-NaN") || resultText.equals("-Infinity")) {
            resultText = "Ошибка";
        }

        return resultText;
    }

    public String Sum(String numberText1, String numberText2) {
        Float number1, number2, result;
        String resultText;
        number1 = Float.valueOf(numberText1);
        number2 = Float.valueOf(numberText2);

        result = number1 + number2;
        resultText = String.valueOf(result);
        if (resultText.equals("NaN") || resultText.equals("Infinity") || resultText.equals("-NaN") || resultText.equals("-Infinity")) {
            resultText = "Ошибка";
        }

        return resultText;
    }

    public String Min(String numberText1, String numberText2) {
        Float number1, number2, result;
        String resultText;
        number1 = Float.valueOf(numberText1);
        number2 = Float.valueOf(numberText2);

        result = number1 - number2;
        resultText = String.valueOf(result);
        if (resultText.equals("NaN") || resultText.equals("Infinity") || resultText.equals("-NaN") || resultText.equals("-Infinity")) {
            resultText = "Ошибка";
        }

        return resultText;
    }

    public String Mul(String numberText1, String numberText2) {
        Float number1, number2, result;
        String resultText;
        number1 = Float.valueOf(numberText1);
        number2 = Float.valueOf(numberText2);

        result = number1 * number2;
        resultText = String.valueOf(result);
        if (resultText.equals("NaN") || resultText.equals("Infinity") || resultText.equals("-NaN") || resultText.equals("-Infinity")) {
            resultText = "Ошибка";
        }

        return resultText;
    }

    public String Sep(String numberText1, String numberText2) {
        Float number1, number2, result;
        String resultText;
        number1 = Float.valueOf(numberText1);
        number2 = Float.valueOf(numberText2);

        result = number1 / number2;
        resultText = String.valueOf(result);
        if (resultText.equals("NaN") || resultText.equals("Infinity") || resultText.equals("-NaN") || resultText.equals("-Infinity")) {
            resultText = "Ошибка";
        }

        return resultText;
    }

    public void AddMinus(View view) {
        if (!text1.getText().equals("Ошибка")) {
            text0 = text1.getText().toString();
            text1.setText("0");
            action = "Minus";
        }
    }

    public void SeparationNumber(View view) {
        if (!text1.getText().equals("Ошибка")) {
            text0 = text1.getText().toString();
            text1.setText("0");
            action = "Sep";
        }
    }

    public void MultiplicationNumber(View view) {
        if (!text1.getText().equals("Ошибка")) {
            text0 = text1.getText().toString();
            text1.setText("0");
            action = "Mul";
        }
    }


    public void AddXa(View view) {
        if (!text1.getText().equals("Ошибка")) {
            text0 = text1.getText().toString();
            text1.setText("0");
            action = "Degree";
        }
    }
}